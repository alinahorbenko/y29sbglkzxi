import React, { Component } from 'react'
import UserInfo from './UserInfo'
import PropertyInfo from './PropertyInfo'
import ToursSchedule from './ToursSchedule'
import Diagram from './Diagram'
import OffersTable from './OffersTable'
import $ from 'jquery'

class UserPage extends Component {

    constructor(props) {
        super(props);
        this.state = {value: ""};
        this.getData = this.getData.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    } 

    getData() {
        $.ajax({
            url: "../json-test-data/propertiesList.json",
            type: 'GET',
            dataType: 'json',
            success: function(parsed_json){
                this.setState({value: parsed_json});
            }.bind(this)
        });
    }

    componentDidMount() {
        this.getData();
    }

    render() {
        if (!this.state.value) {
            return null;
        }
        else {
        return(
            <section className="seller-offers">
                <div className="container center-xy">
                <UserInfo user={this.state.value.user}/>
                <div className="content">
                    <div className="content-top">
                        <PropertyInfo property={this.state.value.user.property} />
                        <ToursSchedule statistics={this.state.value.statistics} />
                        <Diagram diagram={this.state.value.statistics.diagram} /> 
                    </div>
                    <OffersTable properties={this.state.value.properties} />
                </div>
                </div>
                <div className="process-diagram half center center-self">
                    <div className="svg-container">
                        <svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 950 1920">
                            <path fill="none" d="M 150 138 L 150 30 L 400 30"/>
                            <path fill="none" d="M 815 138 L 815 30 L 620 30"/>
                            <path fill="none" d="M 480 138 L 480 50"/>
                        </svg>
                    </div>
                    <div className="diagram">
                        <div className="row">
                            <div className="bg-yellow center">Sale</div>
                        </div>
                        <div className="row space-l">
                            <a href="#documents" className="btn">Documents</a>
                            <a href="#process" className="btn">Process</a>
                            <a href="#offers" className="btn">Offers</a>
                        </div>
                    </div>
                </div>
            </section>
        );
        }
    }
}

export default UserPage;