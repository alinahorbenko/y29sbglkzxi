import React, {Component} from 'react'

class LoginFail extends Component {
    render() {
        return(
            <section className="section login-fail" data-anchor="login-fail" id="login-fail">
            <div className="container center center-xy">
                <h1 className="title center-self bold uppercase">Thank you!</h1>
                <h1 className="center-self">Provided e-mail do not match our records.<br/>
                    Please submit property water bill that confirms your identity</h1><br/>
                <div className="form-container">
                    <form id="login-fail">
                        <div className="btn-container center-self">
                            <button className="upload-btn half">
                                <i className="fa fa-paperclip"></i>Water bill
                            </button>
                            <input type="file" name="file" required/>
                        </div>
                        <button className="bg-yellow center-self half" type="submit">Submit</button>
                    </form>
                </div>
                </div>
        </section>
        ); 
    }
}

export default LoginFail;