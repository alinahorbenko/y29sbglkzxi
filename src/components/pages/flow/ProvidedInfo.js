import React, {Component} from 'react'

class ProvidedInfo extends Component {
    render() {
        return(
            <section className="section property-worth" data-anchor="property-worth" id="property-worth">
                <div className="split-container">
                    <div className="split left">
                        <div className="container full-width">
                            <div className="main-titles-container">
                                <div className="main-titles">
                                    <div className="yellow-square"></div>
                                    <h1>Based on<br/> provided</h1>
                                    <p className="half">
                                        Info your property is worth in the <span className="bold">${this.props.min} - {this.props.max}M range</span><br/>
                                        Collider has identified {this.props.buyers} most likely buyers
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="split right">
                        <div className="button-container bottom-center">
                            <a className="button yellow">Comparables</a>
                            <a className="button transparent">Sell with us?</a>
                        </div>   
                    </div>   
                </div>
            </section>
        )
    }
}

export default ProvidedInfo;