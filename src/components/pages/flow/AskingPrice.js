import React, {Component} from 'react'

function SignButton() {
    return(
        <button id="sign" className="submit-btn bg-yellow half center-self">Sign commission agreement</button>
    )
}

class AskingPrice extends Component {

    constructor(props) {
        super();
        this.state = {
            isPriceTooHigh: true
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const target = e.target;
        this.setState({
          [target.name]: target.value
        });
      }
    
    handleSubmit(e) { //dummy validation
        e.preventDefault();
        var average = 100000;
        if((this.state.price/average) > 1.25) {
            this.props.history.push({
                pathname: "/asking-price-is-too-high"
                }); 
        }
        else {
            this.setState({isPriceTooHigh: false})
        }
    }
    render() {
        return(
            <section className="section asking-price" data-anchor="asking-price" id="asking-price">
                <div className="form-container container">
                    <form id="askprice">
                        <label htmlFor="price">Asking price</label>
                        <input className ="half" type="text" name="price" id="price" required onChange={this.handleChange}/>
                        <div className="btn-container half">
                            <button className="upload-btn">
                                <i className="fa fa-paperclip"></i>Attach rent roll & expenses
                            </button>
                            <input type="file" name="file" required onChange={this.handleChange}/>
                        </div>
                        <label htmlFor="comments">Comments</label>
                        <textarea name="comments" id="comments" onChange={this.handleChange}></textarea>
                        <button className="submit-btn bg-yellow one-third center-self" onClick={this.handleSubmit}>Submit</button><br/>
                        {this.state.isPriceTooHigh ? null : <SignButton />}
                    </form>
                </div>
            </section>
        )
    }
}

export default AskingPrice;