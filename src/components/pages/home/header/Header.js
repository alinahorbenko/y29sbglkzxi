import React, { Component } from "react";
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import logo from '../../../../img/logo.png';
import searchIcon from '../../../../img/search.svg';
import Home from '../menu-items/Home';
import About from '../menu-items/About';
import News from '../menu-items/News';
import Contact from '../menu-items/Contact';
import Main from '../Main'
class Header extends Component {
    render() {
        return(
            <Router>
                <React.Fragment>
                    <header>
                        <Link to="/" className="center-self"><img id="logo" src={logo} alt="logo"/></Link>
                        <nav>
                            <ul>
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/">About</Link></li>
                                <li><Link to="/">News</Link></li>
                                <li><Link to="/">Contact</Link></li>
                            </ul>
                        </nav>
                        <img className="search-bar" src={searchIcon} alt="search icon"/>

                    </header>
                </React.Fragment>
            </Router>
        );
    }
}
  
export default Header; 